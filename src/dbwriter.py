import sqlite3
import datetime
import logging
import sys

class DBWriter:
    """ DB Writer Class """
    """This class opens a db connection and inserts data"""
    def __init__(self, path='db.sqlite'):
        """Init """
        self.log = logging.getLogger('gateway')
        self.path = path
        self.connect()

    def connect(self):
        """Connects to Database"""
        try:
            self.con = sqlite3.connect(self.path)
            self.cur = self.con.cursor()
        except Exception as e:
            self.log.exception(e)
            sys.exit(-1)
    def close(self):
        self.con.close()
        

    def create_temp_table(self):
        """Creates Temperature table for current month to store readings"""
        currentdate = datetime.datetime.now()
        year = "{:02d}".format(currentdate.year)
        month = "{:02d}".format(currentdate.month)

        try:

            self.cur.execute('create table if not exists temperatures_{0}_{1} (id integer primary key autoincrement, devicename varchar (20), cpuid varchar(5) , temperature integer)'.format(month,year))
            
        except sqlite3.Error as e:
            self.log.exception(e)

    def insertData(self,row):
        """Inserts temperature readings into data """
        """Row is a tuple of ( devicename, cpu, temperature) """
        currentdate = datetime.datetime.now()
        year = "{:02d}".format(currentdate.year)
        month = "{:02d}".format(currentdate.month)
        
        try:
            values= row
            cur = self.con.cursor()
            res = cur.execute("insert into temperatures_{0}_{1} (devicename,cpuid,temperature) values (?,?,?)".format(month,year),values)
            self.con.commit()
        except sqlite3.Error as e:
            self.log.exception(e)
        