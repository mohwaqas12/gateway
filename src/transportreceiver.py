import os 
import select
import socket 
import logging 
class TransportReciever:
    """Creates a tcp server, listen for connections and
       recieves data from connected cliends"""

    def __init__(self,host='localhost', port=5005):
        self.log = logging.getLogger('gateway')
        self.host = host
        self.port = port
        self.server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.server.setblocking(0)
        self.server.bind((host,port))
        self.server.listen(4)
        self.log.info('Server listening on %s:%d'% (host, port) )
        self.eventlist = []
        self.eventlist.append(self.server)

    def process(self):
        """Monitors the server and cliends"""
        """Returns received data or None otherwise"""
        inready , outready , exceptionready = select.select( self.eventlist, [], self.eventlist,0.05)
    
        for sock in inready:
            if sock == self.server:
                client , address = self.server.accept()
                self.log.debug('Connection accesspted from %s:%d' % address)
                self.eventlist.append(client)
            else:
                data = sock.recv(500)
                if len(data) == 0:
                    self.log.info('Closing a client' )
                    sock.close()
                    self.eventlist.remove(sock)
                    self.log.info('Total connected %d', len(self.eventlist)-1 )
                    return None
                self.log.debug('Recieved data %s' % (data))
                return data
            
        for sock in exceptionready:
            self.log.debug('Got an exception on socket closing a client' )
            self.eventlist.remove(sock)
            sock.close()



        

