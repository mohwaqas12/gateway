#! /usr/bin/python

import os
import select
import time
import json
import logging 
import logging.handlers
import signal  

from dbwriter import DBWriter
from transportreceiver import TransportReciever

ShouldRun = True

def SignalHandler(Signal,Frame):
    global ShouldRun
    if Signal == signal.SIGTERM:
        ShouldRun = False

def run():
    """Creates a server and listen on socket.
       Write received data to db,
       Runs until SIGTERM is recevied"""
    global ShouldRun
    try:
            
        transport  = TransportReciever()
        dbwriter = DBWriter(path='/var/local/db.sqlite')
        dbwriter.create_temp_table()

        while ShouldRun == True:
            try:
                data =  transport.process()
                if data is not None: 
                    log.debug('Got data : %s' % data)
                    json_data = json.loads(data)
                    tuple_data =  ( 'device0',json_data['cpu'], json_data['temp'] )
                    dbwriter.insertData(tuple_data)
            except Exception as e:
                log.exception(e)
            time.sleep(0.5)

        log.info('Exiting gracefully')
    except Exception as e:
        log.exception(e)

if __name__ == '__main__':
    log = logging.getLogger('gateway')
    log.setLevel(logging.DEBUG)
    handler = logging.handlers.SysLogHandler(address = '/dev/log')
    log.addHandler(handler)
    signal.signal(signal.SIGTERM,SignalHandler)
    log.info("Starting Gateway")    
    run()