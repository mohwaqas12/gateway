import unittest
import sys 
import sqlite3 
import datetime 

sys.path.append('../src')

from  dbwriter import DBWriter

class DBWriter_CreateTable_Test(unittest.TestCase):

    def setUp(self):
        self.dbwriter = DBWriter('db.sqlite')
        self.filename = 'db.sqlite'
        self.con = sqlite3.connect(self.filename)
        self.cursor= self.con.cursor()
        self.currenttime = datetime.datetime.now()
        self.year = "{:02d}".format(self.currenttime.year)
        self.month = "{:02d}".format(self.currenttime.month)
        self.temp_table = "temperatures_{0}_{1}".format(self.month,self.year)

    def test_create_temp_table(self): 
        self.dbwriter.create_temp_table()
        
        self.cursor.execute("select name from sqlite_master where type='table' and name='%s'" % self.temp_table)
        self.assertEqual( self.temp_table ,  str(self.cursor.fetchone()[0]) )

    def tearDown(self):
        cur = self.con.cursor()
        cur.execute("drop table %s" % self.temp_table )
        self.con.close()
        self.dbwriter.close()

class DBWriter_InsertData_Test(unittest.TestCase):

    def setUp(self):
        self.dbwriter = DBWriter('db.sqlite')
        self.filename = 'db.sqlite'
        self.con = sqlite3.connect(self.filename)
        self.cursor= self.con.cursor()
        self.currenttime = datetime.datetime.now()
        self.year = "{:02d}".format(self.currenttime.year)
        self.month = "{:02d}".format(self.currenttime.month)
        self.temp_table = "temperatures_{0}_{1}".format(self.month,self.year)

        self.cursor.execute('create table if not exists %s (id integer primary key autoincrement, devicename varchar (20), cpuid varchar(5) , temperature integer)' % self.temp_table)

    def test_insertdata(self):
        data = (  ( 'device1', 'cpu1', 333 ), ( 'device2', 'cpu2', 444 )  )
        
        for row in data:     
            self.dbwriter.insertData(row)
        cur = self.con.cursor()
        cur.execute("select devicename,cpuid,temperature from %s order by id asc" % self.temp_table )
        result = cur.fetchall()     
        self.assertEqual( 2 , len(result))
    
    def tearDown(self):
        cur = self.con.cursor()
        cur.execute("delete from %s" % self.temp_table )
        cur.execute("drop table %s" % self.temp_table )
        self.con.close()
        self.dbwriter.close()

if __name__ == '__main__':
    unittest.main()