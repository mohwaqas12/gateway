import os
import unittest
from  dbwriter_test import DBWriter_CreateTable_Test
from  dbwriter_test import DBWriter_InsertData_Test


def testSuite():
    suite = unittest.TestSuite()
    suite.addTest(DBWriter_CreateTable_Test('test_create_temp_table') )
    suite.addTest(DBWriter_InsertData_Test('test_insertdata') )

    return suite

if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(testSuite())