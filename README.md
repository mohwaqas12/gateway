#Gateway
This contains gateway source written in python
Gateway reads data over port 5005 and saves them in DB
All output goes to syslog 
By default all data is saved in sqlite file at location `/var/local/db.sqlite`

##Requirenments 
- This requies python to be installed and avaiable in /usr/bin/python
- Requires systemd managar to be installed on system

##Installation
- Run `make install` to install the gateway.
- `Make install` will install and run the gateway

##Usage 
- To start service `systemctl start gateway.service`
- To stop service `systemctl stop gateway.service`
- To disable service  on startup `systemctl disable gateway.service`
- To enable service `systemctl enable gateway.service`
- To view logs `journalctl -f -u gateway.service`


##Improvements
1. Test TransportReciever in unit tests
2. Use argparse to to take command line parammeters
  * Make an option to log to stdout if run in standalone mode
3. Improve TransportReciver to include UDP SOCK or  UNIX sockets
4. Make parameters configurable 

