TARGET = /usr/local/bin/gateway /usr/local/lib/systemd/system/gateway.service
PYTHON_INSTALL = $(shell which python)

install :
        ifneq ($(PYTHON_INSTALL),/usr/bin/python)
               $(error "Python installation not found, Expect python. Please install python")
        endif
	install -d /usr/local/bin/gateway
	install -m 755 src/gateway.py /usr/local/bin/gateway/
	install -m 755 src/dbwriter.py /usr/local/bin/gateway/
	install -m 755 src/transportreceiver.py /usr/local/bin/gateway/
	install -d /usr/local/lib/systemd/s4ystem
	echo "Installing service"
	install -m 644 scripts/gateway.service /usr/local/lib/systemd/system/
	systemctl daemon-reload
	echo "Service enabled"
	systemctl enable gateway.service
	echo "Service started"
	systemctl start gateway
